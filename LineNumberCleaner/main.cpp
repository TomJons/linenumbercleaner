#include <iostream>
#include <fstream>
using namespace std;
#include "Parser.h"


//name of file is the first parameter
//parameters:
// -n  = clear new line space between rows
// -v	= /n = /n not /n = /r/n
// -h  = help :D

bool helpMethod(string str);

int main(int argc, char** argv)
{
	Mode mode = NO_MODE;
	if (argc > 1)
	{
		for (int i = 2; i < argc; i++)
		{
			if (string(argv[i]) == "-n")
			{
				cout << "Czysci puste linie pomiedzy wierszami" << endl;
				mode = CLEAR_NEW_LINES;
			}
			else if (string(argv[i]) == "-v")
			{
				cout << "\\n == \\n not \\n = \\r\\n" << endl;
				mode = (Mode)(mode | VISUAL_LINES);
			}
		}
	}
	else 
	{
		cerr << "Brak nazwy pliku, ktora ma byc 1 parametrem" << endl;
		return 1;
	}

	if (helpMethod(argv[1]))
		return 0;
	try 
	{
		Parser parser(argv[1], mode);
		if (parser.parse())
		{
			cout << "Operacja zakonczona poprawnie" << endl;
			return 0;
		}
	}
	catch (int error_number)
	{
		cerr << "Nie udalo sie sparsowac pliku - BLAD:" << error_number << endl;
		return 1;
	}
}


bool helpMethod(string str)
{
	if (str == "-h")
	{
		string help = "==== POMOC ====\n"
			"nazwa pliku jest pierwszym parametrem\n\n"
			"opcjonalne inne parametry:\n"
			"-n  = Czysci linie pomiedzy wierszami\n"
			"-v	= /n = /n not /n = /r/n - Visual Lines\n"
			"-h  = pomoc :D\n";

		cout << help.c_str() << endl;
		return true;
	}
	else
		return false;
}
