#pragma once

enum Mode { NO_MODE = 0, CLEAR_NEW_LINES = 1, VISUAL_LINES = 2 };

class Parser
{
private:
	std::string nazwaPliku;

	std::ifstream fileIn;
	std::ofstream fileOut;

	Mode mode;

	//func
	void startLine();
	void endLine();
	void defFunction(int c);

public:
	Parser(char* nazwa, Mode m);
	~Parser();
	bool parse();
};

