#include <iostream>
#include <fstream>
using namespace std;
#include "Parser.h"

//throws int - 1 Plik zrodlowy zawiodl
//				2 Plik docelowy zawiodl
Parser::Parser(char* nazwa, Mode m) : nazwaPliku(nazwa)
{
	fileIn.open(nazwaPliku, ios::in);
	if (!fileIn)
		throw 1;
	fileOut.open(nazwaPliku + "2", ios::out);
	if (!fileOut)
		throw 2;
	this->mode = m;
}

Parser::~Parser()
{
	fileIn.close();
	fileOut.close(); 
}

bool Parser::parse()
{
	int c;
	startLine();
	while ((c = fileIn.get()) != EOF)
	{
		switch (c)
		{
		case '\n':
			endLine();
			break;
		
		default:
			defFunction(c);
		}
	}
	return true;
}


/***********************************************/

void Parser::defFunction(int c)
{
	fileOut << (char)c;
}

/***********************************************/

void Parser::endLine()
{
	if (mode & VISUAL_LINES)
		fileOut << "\n";
	else
		fileOut << "\r\n";

	startLine();
}

/***********************************************/

void Parser::startLine()
{
	int c = fileIn.get();
	bool space = true;
	bool colon = false;
	do
	{
		if (isdigit(c))
		{
			space = false;
			colon = true;
		}
		else if (space && (c == ' ' || c == '\t'))
			continue;
		else if (space && c == '\n')
		{
			if (mode & CLEAR_NEW_LINES)
				continue;
			else 
				fileOut << "\n";
		}
		
		else if (colon && c == ':')
			continue;
		else
		{
			fileOut << (char)c;
			break;
		}
			
	} while ((c = fileIn.get()) != EOF);
}
